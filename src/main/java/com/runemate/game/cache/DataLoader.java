package com.runemate.game.cache;

import com.google.common.cache.*;
import com.google.common.primitives.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.cache.file.*;
import java.util.*;
import java.util.concurrent.*;

public class DataLoader {
    protected final int archive;
    private final Cache<Integer, Map<Integer, byte[]>> group_cache =
        CacheBuilder.newBuilder().softValues().expireAfterAccess(5, TimeUnit.MINUTES).build();

    public DataLoader(int archive) {
        this.archive = archive;
    }

    public boolean exists(JS5CacheController js5, int group) {
        if (group_cache.getIfPresent(group) != null) {
            return true;
        }
        Js5IndexFile idxFile = js5.getIndexFile(archive);
        if (idxFile == null) {
            return false;
        }
        return idxFile.getReferenceTable().exists(group);
    }

    public byte[] retrieve(JS5CacheController js5, int group, int file) {
        if (js5 == null || group == -1 || file == -1) {
            return null;
        }
        Map<Integer, byte[]> cached = group_cache.getIfPresent(group);
        if (cached != null) {
            return cached.get(file);
        }
        Js5IndexFile js5Idx = js5.getIndexFile(archive);
        if (js5Idx == null) {
            return null;
        }
        Map<Integer, byte[]> files = js5Idx.getReferenceTable().load(group);
        //crc is checked at the end of this stage
        if (files == null) {
            return null;
        }
        group_cache.put(group, files);
        return files.get(file);
    }

    public byte[] retrieve(JS5CacheController js5, String groupName, int file) {
        if (js5 == null || groupName == null || file == -1) {
            return null;
        }
        Js5IndexFile js5Idx = js5.getIndexFile(archive);
        if (js5Idx == null) {
            return null;
        }
        int group = DJB2Hasher.getGroupId(js5Idx.getReferenceTable(), groupName);
        Map<Integer, byte[]> cached = group_cache.getIfPresent(group);
        if (cached != null && cached.containsKey(file)) {
            return cached.get(file);
        }
        Map<Integer, byte[]> files = js5Idx.getReferenceTable().load(group);
        //crc is checked at the end of this stage
        if (files == null) {
            return null;
        }
        group_cache.put(group, files);
        return files.get(file);
    }

    public int[] groups(JS5CacheController js5) {
        if (js5 == null) {
            return new int[0];
        }
        Js5IndexFile file = js5.getIndexFile(archive);
        if (file == null) {
            return new int[0];
        }
        return file.getReferenceTable().groups();
    }

    public int[] files(JS5CacheController js5, int group) {
        if (js5 == null || group == -1) {
            return new int[0];
        }
        Map<Integer, byte[]> cached = group_cache.getIfPresent(group);
        if (cached != null) {
            return Ints.toArray(cached.keySet());
        }
        Js5IndexFile js5Idx = js5.getIndexFile(archive);
        if (js5Idx == null) {
            return new int[0];
        }
        Map<Integer, byte[]> files = js5Idx.getReferenceTable().load(group);
        //crc is checked at the end of this stage
        if (files == null) {
            return new int[0];
        }
        group_cache.put(group, files);
        return Ints.toArray(files.keySet());
    }
}
