package com.runemate.game.api.hybrid.cache;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import java.awt.*;

public class MapTileColors {
    public static Color calculate(
        int overlayId, int underlayId, int shape, int rotation,
        boolean flat
    ) {
        int underlay_rgb = 0;
        if (underlayId > 0) {
            com.runemate.game.api.hybrid.cache.elements.CacheUnderlayDefinition underlay =
                (CacheUnderlayDefinition) UnderlayDefinitions.load(underlayId - 1);
            //TODO use the averaging they apply to smooth things
            //TODO apply smoothing at hsl level
            underlay_rgb = underlay.rgb_color;
        }
        int overlay_rgb = 0;
        if (overlayId > 0) {
            com.runemate.game.api.hybrid.cache.elements.CacheOverlayDefinition overlay =
                (CacheOverlayDefinition) OverlayDefinitions.load(overlayId - 1);
            Material material = overlay.getMaterial();
            if (material != null) {
                overlay_rgb = material.getColor().getRGB();
            } else if (overlay.base_rgb_color != -1) {
                //TODO smooth at HSL level
                overlay_rgb = overlay.base_rgb_color;
            }
            if (overlay.blendable_rgb_color != -1) {
                //TODO smooth at HSL level
                overlay_rgb = overlay.blendable_rgb_color;
            }
        }
        if (shape == 0) {
            //only use underlay
            return new Color(underlay_rgb);
        } else if (shape == 1) {
            //Only use overlay
            return new Color(overlay_rgb);
        } else {
            //Complex tile, we can probably make it simple though
            throw new UnsupportedOperationException();
        }
    }
}
