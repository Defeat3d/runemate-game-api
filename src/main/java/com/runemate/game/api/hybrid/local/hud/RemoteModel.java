package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.shapes.*;
import com.runemate.game.api.osrs.entities.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.List;
import java.util.*;
import javafx.scene.canvas.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
public class RemoteModel extends Model {

    private final Shape shape;

    public RemoteModel(final OSRSEntity owner, final Shape shape) {
        super(owner);
        this.shape = shape;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Nullable
    @Override
    public InteractablePoint getInteractionPoint() {
        final var area = new ViewportArea(shape).getVisibleArea();
        if (area == null) {
            return null;
        }
        log.trace("Determining interaction point of {} using remote model", owner);
        final var bounds = area.getBounds2D();
        for (int i = 0; i < 50; i++) { //maximum of 50 attempts before falling back to legacy point selection
            var point = new InteractablePoint(
                (int) Random.nextGaussian(bounds.getMinX(), bounds.getMaxX(), bounds.getMinX() + (bounds.getWidth() / 2)),
                (int) Random.nextGaussian(bounds.getMinY(), bounds.getMaxY(), bounds.getMinY() + (bounds.getHeight() / 2))
            );
            if (area.contains(point)) {
                log.trace("Selecting point {}", point);
                return point;
            }
        }
        return super.getInteractionPoint();
    }

    @Override
    public List<Triangle> projectTriangles() {
        final var area = new Area(shape);
        List<Triangle> triangles = new ArrayList<>();
        PathIterator it = area.getPathIterator(null);
        double[] coords = new double[6];
        List<Point> points = new ArrayList<>();
        while (!it.isDone()) {
            int type = it.currentSegment(coords);
            switch (type) {
                case PathIterator.SEG_MOVETO:
                    points.clear();
                    points.add(new Point((int) coords[0], (int) coords[1]));
                    break;
                case PathIterator.SEG_LINETO:
                    points.add(new Point((int) coords[0], (int) coords[1]));
                    break;
                case PathIterator.SEG_CLOSE:
                    triangulatePoints(points, triangles);
                    break;
            }
            it.next();
        }
        return triangles;
    }

    private static void triangulatePoints(List<Point> points, List<Triangle> triangles) {
        if (points.size() < 3) {
            return;
        }
        for (int i = 1; i < points.size() - 1; i++) {
            triangles.add(new Triangle(points.get(0), points.get(i), points.get(i + 1)));
        }
    }

    @Override
    public List<Triangle> projectTrianglesWithin(final Shape viewport) {
        return projectTriangles();
    }

    @Override
    protected int getTriangleCount() {
        return projectTriangles().size();
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Nullable
    @Override
    public BoundingModel getBoundingModel() {
        return null;
    }

    @Override
    public Set<Color> getDefaultColors() {
        return Collections.emptySet();
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void render(Graphics2D g2d) {
        g2d.draw(shape);
    }

    @Override
    public void render(GraphicsContext gc) {
        var bounds = new ViewportArea(shape);
        var iterator = bounds.getPathIterator(null);
        float[] floats = new float[6];
        Point previous = null, current = null;
        Point lineStart = null;
        while (!iterator.isDone()) {
            int type = iterator.currentSegment(floats);
            if (type == PathIterator.SEG_MOVETO) {
                previous = null;
                current = lineStart = new Point((int) floats[0], (int) floats[1]);
            } else if (type == PathIterator.SEG_LINETO) {
                int x = (int) floats[0];
                int y = (int) floats[1];
                if (previous == null || previous.x != x || previous.y != y) {
                    previous = current;
                    current = new Point(x, y);
                    if (previous != null) {
                        gc.strokeLine(previous.x, previous.y, current.x, current.y);
                    }
                }
            } else if (type == PathIterator.SEG_CLOSE) {
                int x = (int) floats[0];
                int y = (int) floats[1];
                if (lineStart != null && (lineStart.x != x || lineStart.y != y)) {
                    gc.strokeLine(lineStart.x, lineStart.y, x, y);
                }
            }
            iterator.next();
        }
    }

    public Shape getShape() {
        return shape;
    }
}
