package com.runemate.game.api.hybrid.region;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.region.*;
import java.util.function.*;

public final class SpotAnimations {
    private SpotAnimations() {
    }

    public static SpotAnimationQueryBuilder newQuery() {
        return new SpotAnimationQueryBuilder();
    }

    public static LocatableEntityQueryResults<SpotAnimation> getLoaded() {
        return getLoaded(null);
    }

    public static LocatableEntityQueryResults<SpotAnimation> getLoaded(
        Predicate<SpotAnimation> filter
    ) {
        return OSRSSpotAnimations.getLoaded(filter);
    }
}
