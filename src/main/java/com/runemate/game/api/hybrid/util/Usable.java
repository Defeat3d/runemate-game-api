package com.runemate.game.api.hybrid.util;

import java.io.*;

/*
 *
 * Note: Don't move this class, everything serialized from it will break!
 */
@FunctionalInterface
public interface Usable extends Serializable {
    boolean isUsable();
}
