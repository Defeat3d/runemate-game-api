package com.runemate.game.api.hybrid;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.osrs.region.*;
import java.util.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

/**
 * Scene represents the loaded 104x104 segment of the game world.
 */
@Log4j2
@UtilityClass
public class Scene {

    public static final int WIDTH = 104;
    public static final int HEIGHT = 104;
    public static final int FLOORS = 4;

    /**
     * By default, looks up the result of the last scene update from {@link com.runemate.game.api.script.framework.listeners.SceneListener}.
     * Otherwise, uses {@link #getCurrentBase()}.
     *
     * @return the base of the current loaded map (plane = local player's plane).
     */
    @NonNull
    public Area.Rectangular getArea() {
        Coordinate base = getBase();
        return new Area.Rectangular(base, base.derive(WIDTH - 1, HEIGHT - 1));
    }

    public static int getCurrentPlane() {
        return OSRSScene.getCurrentPlane();
    }

    /**
     * By default, looks up the result of the last update from {@link com.runemate.game.api.script.framework.listeners.SceneListener}.
     * Otherwise, uses {@link #getCurrentBase()}.
     *
     * @return the base of the current loaded map (plane = local player's plane).
     */
    @NonNull
    public static Coordinate getBase() {
        return OSRSScene.getBase();
    }

    /**
     * By default, looks up the result of the last update from {@link com.runemate.game.api.script.framework.listeners.SceneListener}.
     * Otherwise, uses {@link #getCurrentBase()}.
     *
     * @return the base of the current loaded map (plane = local player's plane).
     */
    @NonNull
    public static Coordinate getBase(int plane) {
        return OSRSScene.getBase(plane);
    }

    public static Coordinate.HighPrecision getHighPrecisionBase() {
        Coordinate base = getBase();
        return new Coordinate.HighPrecision(base.getX() << 7, base.getY() << 7, base.getPlane());
    }

    /**
     * Retrieves the base of the current loaded map directly from the game, updating the cache used by {@link #getBase()}
     * if necessary.
     *
     * @return the base of the current loaded map (plane = local player's plane).
     */
    @NonNull
    public static Coordinate getCurrentBase() {
        //Base can get sometimes get desynced on teleport/death/other (cause unknown), causing functionality that relies on it to fail.
        var base = OSRSScene.getCurrentBase();
        if (!Objects.equals(base, getBase())) {
            log.debug("Current base differs from cached base, updating it.");
            OSRSScene.setStoredBase(base);
        }
        return base;
    }

    public static int[][][] getLoadedChunkIds() {
        return OpenRegion.getInstanceTemplateChunks();
    }

    public static List<Chunk> getLoadedChunks() {
        List<Chunk> templates = new ArrayList<>();
        int[][][] chunks3d = getLoadedChunkIds();
        for (int[][] chunks2d : chunks3d) {
            for (int[] chunks : chunks2d) {
                for (int templateId : chunks) {
                    if (templateId > 0) {
                        templates.add(new Chunk(templateId));
                    }
                }
            }
        }
        return templates;
    }

    @Nullable
    public static Chunk getChunk(int sceneX, int sceneY, int plane) {
        return Chunk.containing(sceneX, sceneY, plane);
    }

    @Nullable
    public static Chunk getChunk(Coordinate loaded) {
        return Chunk.containing(loaded);
    }

    public int[][][] getCollisionFlags() {
        return OpenRegion.getFlags();
    }

    public int[][] getCollisionFlags(int plane) {
        return OpenRegion.getFlags(plane);
    }

    public static byte[][][] getRenderFlags() {
        return OpenRegion.getLandscapeData();
    }

    public static int[][][] getTileHeights() {
        return OpenRegion.getTileHeights();
    }

    public static Set<Integer> getLoadedRegionIds() {
        Set<Integer> regions = new HashSet<>();
        for (int region : OpenRegion.getLoadedRegionIds()) {
            regions.add(region);
        }
        return regions;
    }

    public static List<Entity> getHoveredEntities() {
        return OSRSScene.getHoveredEntities();
    }
}
