package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import java.util.*;
import java.util.function.*;
import org.jetbrains.annotations.*;

public final class InterfaceContainers {
    private InterfaceContainers() {
    }


    public static int getRootIndex() {
        return OpenClient.getHudIndex();
    }

    public static InterfaceContainer getRoot() {
        return getAt(getRootIndex());
    }

    public static boolean areAllLoaded(int... indices) {
        boolean[] loaded = OpenClient.getLoadedInterfaces();
        if (loaded == null) {
            return false;
        }
        for (int index : indices) {
            if (index < 0 || index >= loaded.length || !loaded[index]) {
                return false;
            }
        }
        return true;
    }

    public static boolean areAnyLoaded(int... indices) {
        boolean[] loaded = OpenClient.getLoadedInterfaces();
        if (loaded == null) {
            return false;
        }
        for (int index : indices) {
            if (index >= 0 && index < loaded.length && loaded[index]) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    public static InterfaceContainer getAt(int index) {
        return OSRSInterfaces.getAt(index);
    }

    public static List<InterfaceContainer> getLoaded() {
        return OSRSInterfaces.getLoaded(null);
    }

    public static List<InterfaceContainer> getLoaded(int... indices) {
        return OSRSInterfaces.getLoaded(getIndicesPredicate(indices));
    }

    /**
     * More like is initialized or is prepared. Figure out a better name and make it public again TODO
     */
    @Deprecated
    public static boolean isLoaded(int index) {
        return index >= 0;
    }

    private static Predicate<InterfaceContainer> getIndicesPredicate(int... indices) {
        return interfaceContainer -> {
            Arrays.sort(indices);
            return Arrays.binarySearch(indices, interfaceContainer.getIndex()) >= 0;
        };
    }
}
