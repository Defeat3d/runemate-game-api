package com.runemate.game.api.hybrid.web;

import com.runemate.game.internal.*;

@InternalAPI
public enum ScanAction {
    CONTINUE, STOP
}
