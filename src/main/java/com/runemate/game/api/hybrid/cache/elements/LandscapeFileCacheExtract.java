package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.internal.exception.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import lombok.*;


public class LandscapeFileCacheExtract implements DecodedItem {
    private final Coordinate base;
    private final boolean rs3;
    private Collection<LandscapeObject> objects;

    public LandscapeFileCacheExtract(Coordinate base, boolean rs3) {
        this.base = base;
        this.rs3 = rs3;
    }

    @Override
    public void decode(@NonNull Js5InputStream stream) throws IOException {
        if (stream.size() == 0) {
            return;
        }
        objects = null;
        int untransformed_id = -1;
        while (true) {
            final int id_offset = stream.readUnsignedIntSmartShortCompat();
            if (id_offset == 0) {
                break;
            }
            untransformed_id += id_offset;
            int base_position = 0;
            while (true) {
                final int position_offset = stream.readShortSmart();
                if (position_offset == 0) {
                    break;
                }
                base_position += position_offset - 1;
                int regionY = base_position & 0x3f;
                int regionX = base_position >> 6 & 0x3f;
                int floor = base_position >> 12;

                int shape;
                int orientation;
                if (rs3) {
                    com.runemate.game.api.hybrid.cache.elements.LandscapeObjectPlacementInfo info =
                        new LandscapeObjectPlacementInfo(stream);
                    shape = info.shape;
                    orientation = info.orientation;
                } else {
                    int info = stream.readUnsignedByte();
                    shape = info >> 2;
                    orientation = info & 0x3;
                }
                boolean unrotated = orientation % 2 == 0;
                GameObjectDefinition def = GameObjectDefinition.get(untransformed_id);
                if (def == null) {
                    CacheRegionExceptionLogger.writeException(rs3 ? "rs3" : "osrs",
                        new UnableToLocateCacheFileException(
                            "Failed to load definition for object{id=" + untransformed_id +
                                ", position=(" + (base.getX() + regionX) + ", " +
                                (base.getY() + regionY) + ", " + floor + "), type=" + shape +
                                ", orientation=" + orientation + "}."), base
                    );
                    continue;
                }
                int width = unrotated ? def.getWidth() : def.getHeight();
                int height = unrotated ? def.getHeight() : def.getWidth();
                List<String> actions =
                    def.getActions().stream().filter(action -> !Objects.equals(action, "Examine"))
                        .collect(Collectors.toList());
                if (actions.isEmpty()) {
                    actions = null;
                }
                LandscapeObject object = new LandscapeObject(untransformed_id, def.getName(),
                    regionX, regionY, base.getX() + regionX, base.getY() + regionY, floor,
                    width, height, shape, def.getClippingType(), orientation, def.impassable(),
                    def.impenetrable(),
                    actions
                );

                Collection<GameObjectDefinition> transformation_definitions =
                    def.getTransformations();
                List<LandscapeObject> transformations =
                    new ArrayList<>(transformation_definitions.size());
                for (GameObjectDefinition tdef : transformation_definitions) {
                    int t_width = unrotated ? tdef.getWidth() : tdef.getHeight();
                    int t_height = unrotated ? tdef.getHeight() : tdef.getWidth();
                    List<String> t_actions = tdef.getActions().stream()
                        .filter(action -> !Objects.equals("Examine", action))
                        .collect(Collectors.toList());
                    if (t_actions.isEmpty()) {
                        t_actions = null;
                    }
                    transformations.add(new LandscapeObject(tdef.getId(), tdef.getName(),
                        regionX, regionY, base.getX() + regionX, base.getY() + regionY, floor,
                        t_width, t_height, shape, tdef.getClippingType(), orientation,
                        tdef.impassable(), tdef.impenetrable(),
                        t_actions
                    ));
                }
                if (!transformations.isEmpty()) {
                    object.transformations(transformations);
                    for (LandscapeObject transformation : transformations) {
                        transformation.placeholder(object);
                    }
                }
                if (objects == null) {
                    objects = Collections.newSetFromMap(new ConcurrentHashMap<>(4096));
                }
                objects.add(object);
                objects.addAll(transformations);
            }
        }
    }

    /**
     * Contains objects expected to either be displayed by default or transform from a default object.
     */
    public Collection<LandscapeObject> objects() {
        return objects;
    }

    /**
     * These values are responsible for 13% of memory consumption when building the collision graphs
     */
    public void invalidate() {
        objects.clear();
        objects = null;
    }
}
