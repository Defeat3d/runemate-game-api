package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;

public class Minimap {
    public static boolean isCompassDrawn() {
        return getState() < 3;
    }

    public static void setCompassDrawn(boolean draw) {
        setState(draw, isContentDrawn(), isInteractable());
    }

    public static boolean isInteractable() {
        int state = getState();
        return state == 0 || state == 3;
    }

    public static void setInteractable(boolean interactable) {
        setState(isCompassDrawn(), isContentDrawn(), interactable);
    }

    public static boolean isContentDrawn() {
        int state = getState();
        return state != 2 && state != 5;
    }

    public static void setContentDrawn(boolean draw) {
        setState(isCompassDrawn(), draw, isInteractable());
    }


    private static int getState() {
        return OpenClient.getMinimapState();
    }


    private static void setState(
        boolean drawCompass, boolean drawMinimapContents,
        boolean usableMinimap
    ) {
        int state = 0;
        if (!drawCompass) {
            state += 3;
        }
        if (!drawMinimapContents) {
            state += 1;
        }
        if (!usableMinimap) {
            state += 1;
        }
        OpenClient.setMinimapState(state);
    }
}
