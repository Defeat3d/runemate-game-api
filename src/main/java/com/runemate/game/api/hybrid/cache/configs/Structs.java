package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.cache.*;
import com.google.common.cache.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;

@Log4j2
@UtilityClass
public class Structs {

    private static final StructLoader LOADER = new StructLoader();
    private static final Cache<Integer, Struct> CACHE = CacheBuilder.newBuilder()
        .maximumSize(100)
        .expireAfterAccess(5, TimeUnit.MINUTES)
        .build();

    public static Struct load(int row) {
        return load(JS5CacheController.getLargestJS5CacheController(), row);
    }

    public static Struct load(JS5CacheController controller, int row) {
        if (row >= 0) {
            var def = CACHE.getIfPresent(row);
            if (def == null) {
                try {
                    def = LOADER.load(controller, ConfigType.STRUCT, row);
                } catch (IOException e) {
                    log.warn("Failed to load DBRow {}", row, e);
                }
            }
            if (def != null) {
                CACHE.put(row, def);
            }
            return def;
        }
        return null;
    }

    public static List<Struct> loadAll() {
        return loadAll(null);
    }

    public static List<Struct> loadAll(final Predicate<Struct> filter) {
        var controller = JS5CacheController.getLargestJS5CacheController();
        var quantity = LOADER.getFiles(controller, ConfigType.STRUCT).length;
        var tables = new ArrayList<Struct>(quantity);
        for (int id = 0; id <= quantity; id++) {
            var loaded = load(controller, id);
            if (loaded != null && (filter == null || filter.test(loaded))) {
                tables.add(loaded);
            }
        }
        tables.trimToSize();
        return tables;
    }

}
