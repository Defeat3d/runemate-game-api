package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.osrs.local.hud.interfaces.*;

/**
 * An API that provides a limited abstraction of the ControlPanelTab, ActionWindow, and LegacyTab classes.
 */
public final class InterfaceWindows {
    private InterfaceWindows() {
    }

    public static Openable getEquipment() {
        return ControlPanelTab.EQUIPMENT;
    }

    public static Openable getInventory() {
        return ControlPanelTab.INVENTORY;
    }

    public static Openable getMagic() {
        return ControlPanelTab.MAGIC;
    }

    public static Openable getPrayer() {
        return ControlPanelTab.PRAYER;
    }

    public static Openable getSkills() {
        return ControlPanelTab.SKILLS;
    }

    public static Openable getQuestList() {
        //TODO Implement ActionWindow.QUEST_LIST
        return ControlPanelTab.QUEST_LIST;
    }

    public static Openable getEmotes() {
        return ControlPanelTab.EMOTES;
    }

    // TODO deprecate
    public static Openable getMinigames() {
        return ControlPanelTab.GROUPING;
    }
}
