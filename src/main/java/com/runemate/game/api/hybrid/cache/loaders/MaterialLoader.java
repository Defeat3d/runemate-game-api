package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class MaterialLoader extends SerializedFileLoader<CacheMaterial> {

    public MaterialLoader() {
        super(9);
    }

    @Override
    protected CacheMaterial construct(int entry, int file, Map<String, Object> arguments) {
        return new OSRSCacheMaterial(file);
    }
}
