package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class PlayerQueryBuilder extends ActorQueryBuilder<Player, PlayerQueryBuilder> {
    private Pattern[] wearingAnyOfItems, wearingAllOfItems;
    private final EnumMap<Equipment.Slot, Pattern> wearingItemsInSlots =
        new EnumMap<>(Equipment.Slot.class);
    private Integer minCombatLevel, maxCombatLevel;
    private Integer minTotalLevel, maxTotalLevel;
    private Integer teamId;
    private Boolean female;

    public PlayerQueryBuilder combatLevel(int exactLevel) {
        return combatLevels(exactLevel, exactLevel);
    }

    public PlayerQueryBuilder combatLevels(int minimumLevel, int maximumLevel) {
        this.minCombatLevel = minimumLevel;
        this.maxCombatLevel = maximumLevel;
        return get();
    }

    public PlayerQueryBuilder totalLevel(int exactLevel) {
        return totalLevels(exactLevel, exactLevel);
    }

    public PlayerQueryBuilder totalLevels(int minimumLevel, int maximumLevel) {
        this.minTotalLevel = minimumLevel;
        this.maxTotalLevel = maximumLevel;
        return get();
    }

    public PlayerQueryBuilder teamId(int teamId) {
        this.teamId = teamId;
        return get();
    }

    public PlayerQueryBuilder female(boolean female) {
        this.female = female;
        return get();
    }

    public PlayerQueryBuilder wearingAllOf(String... items) {
        return wearingAllOf(Regex.getPatternsForExactStrings(items).toArray(new Pattern[0]));
    }

    public PlayerQueryBuilder wearingAllOf(Pattern... items) {
        wearingAllOfItems = items;
        return get();
    }

    public PlayerQueryBuilder wearingAnyOf(String... items) {
        return wearingAnyOf(Regex.getPatternsForExactStrings(items).toArray(new Pattern[0]));
    }

    public PlayerQueryBuilder wearingAnyOf(Pattern... items) {
        wearingAnyOfItems = items;
        return get();
    }

    public PlayerQueryBuilder wearing(Equipment.Slot slot, String item) {
        return wearing(slot, Regex.getPatternForExactString(item));
    }

    public PlayerQueryBuilder wearing(Equipment.Slot slot, Pattern item) {
        wearingItemsInSlots.put(slot, item);
        return get();
    }

    @Override
    public PlayerQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends Player>> getDefaultProvider() {
        return () -> Players.getLoaded().asList();
    }

    @Override
    public boolean accepts(Player argument) {
        //TODO
        if (minCombatLevel != null && maxCombatLevel != null) {
            int combatLevel = argument.getCombatLevel();
            if (combatLevel < minCombatLevel || combatLevel > maxCombatLevel) {
                return false;
            }
        }
        if (minTotalLevel != null && maxTotalLevel != null) {
            int totalLevel = argument.getTotalLevel();
            if (totalLevel < minTotalLevel || totalLevel > maxTotalLevel) {
                return false;
            }
        }
        if (teamId != null && teamId != argument.getTeamId()) {
            return false;
        }
        if (female != null && female != argument.isFemale()) {
            return false;
        }
        if (wearingAnyOfItems != null) {
            final List<ItemDefinition> wornItems = argument.getWornItems();
            boolean wears = false;
            for (ItemDefinition wornItem : wornItems) {
                for (Pattern wearingAnyOfItem : wearingAnyOfItems) {
                    if (wearingAnyOfItem.matcher(wornItem.getName()).matches()) {
                        wears = true;
                    }
                }
            }
            if (!wears) {
                return false;
            }
        }
        if (wearingAllOfItems != null) {
            final List<ItemDefinition> wornItems = argument.getWornItems();
            for (Pattern wearingAllOf : wearingAllOfItems) {
                boolean wears = false;
                for (ItemDefinition wornItem : wornItems) {
                    if (wearingAllOf.matcher(wornItem.getName()).matches()) {
                        wears = true;
                    }
                }
                if (!wears) {
                    return false;
                }
            }
        }
        if (wearingItemsInSlots != null && !wearingItemsInSlots.isEmpty()) {
            for (Map.Entry<Equipment.Slot, Pattern> entry : wearingItemsInSlots.entrySet()) {
                SpriteItem itemIn = Equipment.getItemIn(entry.getKey());
                if (itemIn == null ||
                    entry.getValue().matcher(itemIn.getDefinition().getName()).matches()) {
                    return false;
                }
            }
        }
        return super.accepts(argument);
    }
}