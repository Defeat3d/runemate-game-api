package com.runemate.game.api.hybrid.web;

import com.runemate.game.api.hybrid.web.vertex.*;
import com.runemate.game.internal.*;
import lombok.*;
import org.jetbrains.annotations.*;

@Value
@InternalAPI
public class ScanResult {
    @Nullable Vertex vertex;
    @NonNull ScanAction action;
}
