package com.runemate.game.api.hybrid.cache.materials;

import java.awt.*;

public interface Material {
    int getId();

    Color getColor();

    //boolean supportsHighDynamicRangeLighting();

    //byte getEffectType();

    int getMipmapLevel();

    int getEdgeLength();
}
