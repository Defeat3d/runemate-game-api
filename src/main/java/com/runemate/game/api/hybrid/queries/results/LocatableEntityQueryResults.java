package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.util.*;
import java.util.concurrent.*;
import org.jetbrains.annotations.*;

public class LocatableEntityQueryResults<T extends LocatableEntity>
    extends InteractableQueryResults<T, LocatableEntityQueryResults<T>> {
    public LocatableEntityQueryResults(final Collection<? extends T> results) {
        super(results);
    }

    public LocatableEntityQueryResults(
        final Collection<? extends T> results,
        ConcurrentMap<String, Object> cache
    ) {
        super(results, cache);
    }

    @Override
    protected LocatableEntityQueryResults<T> get() {
        return this;
    }

    /**
     * Calls sort(Comparator) with a Comparator that will sort the list by distance from the local player (nearest first)
     */
    public final LocatableEntityQueryResults<T> sortByDistance() {
        return sortByDistanceFrom(Players.getLocal());
    }

    public final LocatableEntityQueryResults<T> sortByDistance(Distance.Algorithm algorithm) {
        return sortByDistanceFrom(Players.getLocal(), algorithm);
    }

    /**
     * Calls sort(Comparator) with a Comparator that will sort the list by distance from the local player (nearest first)
     */
    public final LocatableEntityQueryResults<T> sortByDistanceFrom(Locatable center) {
        return sort(new CachingDistanceComparator(center, Distance.Algorithm.MANHATTAN));
    }

    public final LocatableEntityQueryResults<T> sortByDistanceFrom(
        Locatable center,
        Distance.Algorithm algorithm
    ) {
        return sort(new CachingDistanceComparator(center, algorithm));
    }

    /**
     * Gets the nearest entity to the local player.
     * If two or more entities are tied for the place of nearest then one will be selected according to the active player sense profile.
     */
    @Nullable
    public final T nearest() {
        return nearest(Distance.Algorithm.MANHATTAN);
    }

    @Nullable
    public final T nearest(Distance.Algorithm algorithm) {
        if (size() <= 1) {
            return first();
        }
        return nearestTo(Players.getLocal(), algorithm);
    }

    /**
     * Gets the nearest entity to the given locatable.
     * If two or more entities are tied for the place of nearest then one will be selected according to the active player sense profile.
     */
    @Nullable
    public final T nearestTo(final Locatable locatable) {
        return nearestTo(locatable, Distance.Algorithm.MANHATTAN);
    }

    @Nullable
    public final T nearestTo(final Locatable locatable, Distance.Algorithm algorithm) {
        if (size() <= 1) {
            return first();
        }
        if (locatable == null) {
            return null;
        }
        List<T> sorted = Sort.byDistanceFrom(locatable, this.backingList, algorithm);
        List<T> equidistant = new ArrayList<>(3);
        double minimum = -1;
        HashMap<String, Object> cache = new HashMap<>(4);
        for (T entity : sorted) {
            double distance = Distance.between(locatable, entity, algorithm, cache);
            if (minimum == -1) {
                minimum = distance;
            } else if (minimum != distance) {
                break;
            }
            equidistant.add(entity);
        }
        T preferred = null;
        minimum = -1;
        int tiebreaker = PlayerSense.getAsInteger(PlayerSense.Key.DISTANCE_ANGLE_TIE_BREAKER);
        for (T entity : equidistant) {
            int distance = CommonMath.getDistanceBetweenAngles(
                tiebreaker,
                CommonMath.getAngleOf(locatable, entity)
            );
            if (minimum == -1 || distance < minimum) {
                minimum = distance;
                preferred = entity;
            }
        }
        return preferred;
    }

    /**
     * Gets the furthest entity to the local player.
     * If two or more entities are tied for the place of furthest then one will be selected according to the active player sense profile.
     */
    @Nullable
    public final T furthest() {
        if (size() <= 1) {
            return last();
        }
        return furthestFrom(Players.getLocal());
    }

    @Nullable
    public final T furthestFrom(final Locatable locatable) {
        return furthestFrom(locatable, Distance.Algorithm.MANHATTAN);
    }

    @Nullable
    public final T furthestFrom(final Locatable locatable, Distance.Algorithm algorithm) {
        if (size() <= 1) {
            return last();
        }
        if (locatable == null) {
            return null;
        }
        List<T> sorted = Sort.byDistanceFrom(locatable, this.backingList);
        Collections.reverse(sorted);
        List<T> equidistant = new ArrayList<>(3);
        double maximum = -1;
        HashMap<String, Object> cache = new HashMap<>();
        for (T entity : sorted) {
            double distance = Distance.between(locatable, entity, algorithm, cache);
            if (maximum == -1) {
                maximum = distance;
            } else if (maximum != distance) {
                break;
            }
            equidistant.add(entity);
        }
        int tiebreaker;
        if (equidistant.size() > 1 && locatable.equals(Players.getLocal())) {
            List<T> visible = new ArrayList<>(3);
            tiebreaker = PlayerSense.getAsInteger(PlayerSense.Key.DISTANCE_VISIBILITY_TIE_BREAKER);
            for (T entity : equidistant) {
                if (entity.getVisibility() >= tiebreaker) {
                    visible.add(entity);
                }
            }
            if (!visible.isEmpty()) {
                equidistant = visible;
            }
        }
        tiebreaker = PlayerSense.getAsInteger(PlayerSense.Key.DISTANCE_ANGLE_TIE_BREAKER);
        T preferred = null;
        double minimum = -1;
        for (T entity : equidistant) {
            int distance = CommonMath.getDistanceBetweenAngles(
                tiebreaker,
                CommonMath.getAngleOf(locatable, entity)
            );
            if (minimum == -1 || distance < minimum) {
                minimum = distance;
                preferred = entity;
            }
        }
        return preferred;
    }
}
