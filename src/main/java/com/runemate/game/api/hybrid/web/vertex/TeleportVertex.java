package com.runemate.game.api.hybrid.web.vertex;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.web.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

@ToString
@RequiredArgsConstructor
public abstract class TeleportVertex implements Vertex {

    @NonNull
    protected final Coordinate position;

    @NonNull
    @Override
    public  Coordinate getPosition() {
        return position;
    }

    @Nullable
    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition() {
        return position.getHighPrecisionPosition();
    }

    @Nullable
    @Override
    public Area.Rectangular getArea() {
        return position.getArea();
    }

    @Override
    public ScanResult scan(final Map<String, Object> cache) {
        return new ScanResult(this, ScanAction.CONTINUE);
    }
}
