package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.rs3.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class DialogItemTeleportVertex extends ItemTeleportVertex
    implements SerializableRequirement {

    private int id = -1;
    private Pattern name;
    private Pattern action;
    private SpriteItem.Origin origin;
    private boolean click_continue;
    private int delay_length;
    private Pattern dialog_option;

    public DialogItemTeleportVertex(
        int x, int y, int plane, SpriteItem.Origin origin,
        Pattern name, Pattern action, Pattern dialog_option,
        boolean click_continue, int delay_length,
        Collection<WebRequirement> conditions
    ) {
        this(new Coordinate(x, y, plane), origin, name, action, dialog_option, click_continue,
            delay_length, conditions
        );
    }

    public DialogItemTeleportVertex(
        Coordinate position, SpriteItem.Origin origin,
        Pattern name, Pattern action, Pattern dialog_option,
        boolean click_continue, int delay_length,
        Collection<WebRequirement> conditions
    ) {
        super(position, conditions);
        this.origin = origin;
        this.name = name;
        this.action = action;
        this.dialog_option = dialog_option;
        this.click_continue = click_continue;
        this.delay_length = delay_length;
    }

    public DialogItemTeleportVertex(
        Coordinate position, SpriteItem.Origin origin,
        int id, Pattern action, Pattern dialog_option,
        boolean click_continue, int delay_length,
        Collection<WebRequirement> conditions
    ) {
        super(position, conditions);
        this.origin = origin;
        this.id = id;
        this.action = action;
        this.dialog_option = dialog_option;
        this.click_continue = click_continue;
        this.delay_length = delay_length;
    }

    public DialogItemTeleportVertex(
        int x, int y, int plane, SpriteItem.Origin origin,
        String name, String action, Pattern dialog_option,
        boolean click_continue,
        int delay_length, Collection<WebRequirement> conditions
    ) {
        this(new Coordinate(x, y, plane), origin, Regex.getPatternForExactString(name),
            Regex.getPatternForExactString(action), dialog_option, click_continue, delay_length,
            conditions
        );
    }

    public DialogItemTeleportVertex(
        Coordinate position, SpriteItem.Origin origin,
        String name, String action, Pattern dialog_option,
        boolean click_continue,
        int delay_length, Collection<WebRequirement> conditions
    ) {
        this(position, origin, Regex.getPatternForExactString(name),
            Regex.getPatternForExactString(action), dialog_option, click_continue, delay_length,
            conditions
        );
    }

    public DialogItemTeleportVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public Pattern getAction() {
        return action;
    }

    public int getDelayLength() {
        return delay_length;
    }

    public Pattern getDialogOption() {
        return dialog_option;
    }

    public boolean shouldClickContinue() {
        return click_continue;
    }

    @Override
    public SpriteItem getItem() {
        final SpriteItemQueryBuilder builder;
        switch (origin) {
            case EQUIPMENT:
                builder = Equipment.newQuery();
                break;
            case INVENTORY:
                builder = Inventory.newQuery();
                break;
            case BANK:
                builder = Bank.newQuery();
                break;
            default:
                throw new UnsupportedOperationException(
                    "Items in " + origin + " are not supported by DialogItemTeleportVertex");
        }
        if (id != -1) {
            builder.ids(id);
        }
        if (name != null) {
            builder.names(name);
        }
        if (action != null) {
            builder.actions(action);
        }
        if (provider != null) {
            builder.provider(() -> provider);
        }
        return builder.results().first();
    }

    @Override
    public void invalidateCache() {
        provider = null;
    }

    @Override
    public Predicate<SpriteItem> getFilter() {
        final SpriteItemQueryBuilder builder;
        switch (origin) {
            case EQUIPMENT:
                builder = Equipment.newQuery();
                break;
            case INVENTORY:
                builder = Inventory.newQuery();
                break;
            case BANK:
                builder = Bank.newQuery();
                break;
            default:
                throw new UnsupportedOperationException(
                    "Items in " + origin + " are not supported by DialogItemTeleportVertex");
        }
        if (id != -1) {
            builder.ids(id);
        }
        if (name != null) {
            builder.names(name);
        }
        if (action != null) {
            builder.actions(action);
        }
        return builder::accepts;
    }

    public int getId() {
        return id;
    }

    public Pattern getName() {
        return name;
    }

    @Override
    public int getOpcode() {
        return 10;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(getOrigin().name())
            .append(getName() != null ? getName().pattern() : getId()).append(getAction().pattern())
            .append(getDialogOption() != null ? getDialogOption().pattern() : getDialogOption())
            .append(getDelayLength()).toHashCode();
    }

    @Override
    public String toString() {
        Coordinate pos = getPosition();
        return "DialogItemTeleportVertex(origin=" + getOrigin() + ", name=" + getName() +
            ", action=" + getAction() +
            ", dialogOption=" + getDialogOption() + ", clickContinue=" + shouldClickContinue() +
            ", delayLength=" + getDelayLength() + ", x=" + pos.getX() + ", y=" + pos.getY() +
            ", plane=" + pos.getPlane();
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(origin.name());
        stream.writeInt(id);
        if (this.id == -1) {
            stream.writeUTF(name.pattern());
            stream.writeInt(name.flags());
        }
        stream.writeUTF(action.pattern());
        stream.writeInt(action.flags());
        stream.writeUTF(dialog_option == null ? "null" : dialog_option.pattern());
        stream.writeInt(dialog_option == null ? 0 : dialog_option.flags());
        stream.writeUTF(Boolean.toString(click_continue));
        stream.writeInt(0);
        stream.writeUTF("null");
        stream.writeInt(0);
        stream.writeInt(delay_length);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.origin = SpriteItem.Origin.valueOf(stream.readUTF());
        if (protocol >= 14) {
            this.id = stream.readInt();
            if (this.id == -1) {
                name = Pattern.compile(stream.readUTF(), stream.readInt());
            }
        } else {
            name = Pattern.compile(stream.readUTF(), stream.readInt());
        }
        this.action = Pattern.compile(stream.readUTF(), stream.readInt());
        String pattern = stream.readUTF();
        int flags = stream.readInt();
        this.dialog_option =
            Objects.equals("null", pattern) ? null : Pattern.compile(pattern, flags);
        this.click_continue = Boolean.valueOf(stream.readUTF());
        stream.readInt();
        stream.readUTF();
        stream.readInt();
        this.delay_length = stream.readInt();
        return true;
    }

    public SpriteItem.Origin getOrigin() {
        return origin;
    }

    @Override
    public boolean step() {
        if (Bank.isOpen()) {
            return Bank.close();
        }
        if (GrandExchange.isOpen()) {
            return GrandExchange.close();
        }
        if (DepositBox.isOpen()) {
            return DepositBox.close();
        }

        final var local = Players.getLocal();
        if (local == null) {
            return false;
        }

        final var start = local.getPosition();
        if (start == null) {
            return false;
        }

        if (isDialogAvailable()) {
            return handleDialog()
                && Execution.delayWhile(() -> start.equals(local.getPosition()), () -> local.getAnimationId() != -1, 1800);
        }

        final var item = getItem();
        return item != null && item.interact(action, name) && Execution.delayUntil(this::isDialogAvailable, 2000, 3000);
    }

    private boolean isDialogAvailable() {
        return ChatDialog.getContinue() != null
            || ChatDialog.getOption(dialog_option) != null
            || !Interfaces.newQuery()
            .containers(187)
            .types(InterfaceComponent.Type.LABEL)
            .texts(dialog_option)
            .results()
            .isEmpty();
    }

    private boolean handleDialog() {
        ChatDialog.Selectable selectable;
        if ((selectable = ChatDialog.getContinue()) != null) {
            return selectable.select();
        } else if ((selectable = ChatDialog.getOption(dialog_option)) != null) {
            return selectable.select();
        }

        var iface = Interfaces.newQuery()
            .containers(187)
            .types(InterfaceComponent.Type.LABEL)
            .texts(dialog_option)
            .results().first();

        return iface != null && iface.click();
    }
}
