package com.runemate.game.api.hybrid.local.hud.interfaces;

public interface Closeable {
    boolean close();

    boolean isClosed();
}
