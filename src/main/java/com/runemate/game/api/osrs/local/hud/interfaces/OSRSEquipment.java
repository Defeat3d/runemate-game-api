package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;

public final class OSRSEquipment {
    private OSRSEquipment() {
    }

    public static InteractableRectangle getBoundsOf(final Equipment.Slot slot) {
        final InterfaceComponent component = slot.getComponent();
        return component != null ? component.getBounds() : null;
    }

    public static boolean isEquipYourCharacterOpen() {
        InterfaceComponent component = Interfaces.newQuery()
            .containers(84).types(InterfaceComponent.Type.LABEL)
            .grandchildren(false).texts("Equip Your Character...")
            .results().first();
        return component != null && component.isVisible();
    }
}
