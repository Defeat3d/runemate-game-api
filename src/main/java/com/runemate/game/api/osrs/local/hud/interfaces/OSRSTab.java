package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import java.awt.event.*;

@Deprecated
public enum OSRSTab implements Tab {
    COMBAT_OPTIONS(KeyEvent.VK_F1, "Combat Options"),
    STATS(KeyEvent.VK_F2, "Stats"),
    QUEST_LIST(KeyEvent.VK_F3, "Quest List", "Minigames", "Achievement Diaries", "Kourend Tasks"),
    INVENTORY(KeyEvent.VK_ESCAPE, "Inventory") {
        @Override
        public boolean isOpen() {
            final InterfaceComponent component = getComponent();
            return component != null && component.getSpriteId() != -1
                || Bank.isOpen()
                || Shop.isOpen()
                || DepositBox.isOpen()
                || OSRSEquipment.isEquipYourCharacterOpen();
        }
    },
    EQUIPMENT(KeyEvent.VK_F4, "Worn Equipment"),
    PRAYER(KeyEvent.VK_F5, "Prayer"),
    MAGIC(KeyEvent.VK_F6, "Magic"),
    CLAN_CHAT(KeyEvent.VK_F7, "Clan Chat"),
    FRIENDS_LIST(KeyEvent.VK_F8, "Friends List"),
    IGNORE_LIST(KeyEvent.VK_F9, "Ignore List"),
    LOGOUT(-1, "Logout"),
    OPTIONS(KeyEvent.VK_F10, "Options"),
    EMOTES(KeyEvent.VK_F11, "Emotes"),
    MUSIC_PLAYER(KeyEvent.VK_F12, "Music Player");
    private final String[] actions;
    private final int hotkey;

    OSRSTab(final int hotkey, final String... actions) {
        this.hotkey = hotkey;
        this.actions = actions;
    }

    public static OSRSTab getOpened() {
        for (final OSRSTab tab : values()) {
            if (tab.isOpen()) {
                return tab;
            }
        }
        return null;
    }

    @OSRSOnly
    public InterfaceComponent getComponent() {
        return Interfaces.newQuery().grandchildren(false)
            .containers(InterfaceContainers.getRootIndex()).actions(actions).results()
            .first();
    }

    @Override
    public boolean open() {
        return open(!PlayerSense.getAsBoolean(PlayerSense.Key.USE_MISC_HOTKEYS));
    }

    @Override
    public boolean isOpen() {
        return isOpen(getComponent());
    }

    public boolean open(final boolean forceClick) {
        final InterfaceComponent component = getComponent();
        if (isOpen(component)) {
            return true;
        }
        if (hotkey != -1 && !forceClick) {
            return Keyboard.typeKey(hotkey) && Execution.delayUntil(() -> isOpen(component), 2000);
        }

        return component != null && component.interact(Regex.getPatternForExactStrings(actions))
            && Execution.delayUntil(() -> isOpen(component), 2000);
    }

    @Override
    public String toString() {
        return "OSRSTab." + name();
    }

    private boolean isOpen(InterfaceComponent component) {
        return component != null && component.getSpriteId() != -1 && !Bank.isOpen();
    }
}
