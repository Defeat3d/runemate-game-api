package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.*;


public final class OSRSInterfaceOptions {
    private OSRSInterfaceOptions() {
    }

    public static boolean canChatboxBeClickedThrough() {
        Varbit bits = Varbits.load(2570);
        return bits != null && bits.getValue() == 0;
    }

    public static TabLayout getTabLayout() {
        Varbit bits = Varbits.load(4607);
        if (bits != null) {
            int value = bits.getValue();
            if (value == 0) {
                return TabLayout.BOX;
            } else {
                return TabLayout.LINE;
            }
        }
        return null;
    }


    public static ViewportMode getViewportMode() {
        return OpenClient.getViewportWidth() == 512 ? ViewportMode.STATIC :
            ViewportMode.RESIZABLE;
    }

    public static boolean isChatboxTransparent() {
        Varbit bits = Varbits.load(4608);
        return bits != null && bits.getValue() == 1;
    }

    public static boolean isViewportResizable() {
        return getViewportMode() == ViewportMode.RESIZABLE;
    }

    public static boolean isViewportStatic() {
        return getViewportMode() == ViewportMode.STATIC;
    }

    public enum TabLayout {
        BOX,
        LINE
    }

    public enum ViewportMode {
        STATIC,
        RESIZABLE
    }
}
