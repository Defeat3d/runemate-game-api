package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.location.*;
import lombok.*;

/**
 * @deprecated replaced by {@link SceneUpdatedEvent}
 */
@Value
@Deprecated
public class RegionLoadedEvent implements Event {

    Coordinate previousBase;
    Coordinate currentBase;
}
