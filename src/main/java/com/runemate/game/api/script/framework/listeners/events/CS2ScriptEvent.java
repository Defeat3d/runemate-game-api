package com.runemate.game.api.script.framework.listeners.events;

import lombok.*;

@Value
public class CS2ScriptEvent implements Event {

    int id;
    Type type;
    Object[] arguments;

    public enum Type {
        STARTED//, FINISHED
    }
}
