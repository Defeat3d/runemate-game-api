package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

/**
 * @deprecated replaced by {@link SceneListener}
 */
@Deprecated
public interface RegionListener extends EventListener {
    void onRegionLoaded(RegionLoadedEvent event);
}
