package com.runemate.game.api.script.framework.logger;

import java.util.*;

@Deprecated
public interface BotLoggerListener extends EventListener {
    void logged(LoggedMessageEvent message);
}
