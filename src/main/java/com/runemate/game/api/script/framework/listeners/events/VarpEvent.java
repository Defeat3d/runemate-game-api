package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.*;
import lombok.*;

@Value
public class VarpEvent implements Event {
    Varp varp;
    int oldValue;
    int newValue;
}
