package com.runemate.game.api.rs3.local.hud.eoc;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.rs3.queries.*;
import com.runemate.game.api.rs3.queries.results.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import org.jetbrains.annotations.*;

@Deprecated
public final class ActionBar {

    private ActionBar() {
    }

    public static boolean isLocked() {
        return false;
    }

    public static boolean isExpanded() {
        return false;
    }

    public static boolean toggleExpansion() {
        return false;
    }

    public static boolean toggleLock() {
        return false;
    }

    public static boolean isAutoRetaliating() {
        return false;
    }

    public static boolean toggleAutoRetaliation() {
        return false;
    }

    public static ActionBarQueryBuilder newQuery() {
        return new ActionBarQueryBuilder();
    }

    /**
     * @deprecated Use the following as an alternative ActionBar#newQuery()#filled(true)#empty(false)#results()
     */
    @Deprecated
    public static ActionBarQueryResults getFilledSlots() {
        return newQuery().filled(true).empty(false).results();
    }

    /**
     * @deprecated Use the following as an alternative ActionBar#newQuery()#filled(false)#empty(true)#results()
     */
    @Deprecated
    public static ActionBarQueryResults getEmptySlots() {
        return newQuery().filled(false).empty(true).results();
    }

    /**
     * Gets the number of the currently opened action bar.
     */
    public static int getNumber() {
        return 0;
    }

    /**
     * Gets the length of the revolution area on the action bar (as in the amount of slots that can have their abilities automatically activated)
     */
    public static int getRevolutionAreaLength() {
        return 0;
    }

    @Deprecated
    public static class Slot implements Interactable, Renderable {

        private final int bar;
        private final int index;
        private final int id;
        private final ContentType type;

        public Slot(int bar, int index, int id, ContentType type) {
            this.bar = bar;
            this.index = index;
            this.id = id;
            this.type = type;
        }

        public int getActionBar() {
            return bar;
        }

        public int getIndex() {
            return index;
        }

        public int getId() {
            return id;
        }

        public ContentType getType() {
            return type;
        }

        public String getName() {
            return null;
        }

        public List<String> getActions() {
            return Collections.emptyList();
        }

        public boolean isEmpty() {
            return false;
        }

        public String getKeyBind() {
            return null;
        }

        public ItemDefinition getItemDefinition() {
            return null;
        }

        public boolean isSelected() {
            return false;
        }

        /**
         * Checks if the slot is both activatable and not cooling down
         */
        public boolean isReady() {
            return false;
        }

        public boolean isActivatable() {
            return false;
        }

        public boolean isCoolingDown() {
            return false;
        }

        /**
         * Activates the action/item in the given spot by either clicking or using the hotkey (decided by player sense)
         *
         * @return true if successfully activated.
         */
        public boolean activate() {
            return false;
        }

        /**
         * Activates the action/item in the given spot by either clicking or using the hotkey (specified)
         *
         * @param click whether to click or use the hotkey
         * @return true if successfully activated.
         */
        public boolean activate(final boolean click) {
            return false;
        }

        public boolean canAutomaticallyActivate() {
            return false;
        }

        public InteractableRectangle getBounds() {
            return null;
        }

        @Deprecated
        public InterfaceComponent getComponent() {
            return null;
        }

        @Override
        public String toString() {
            return "ActionBar.Slot{index=" + index + ", name=" + getName() + '}';
        }

        @Override
        public boolean isVisible() {
            return false;
        }

        @Override
        public double getVisibility() {
            return 0d;
        }

        @Override
        public boolean hasDynamicBounds() {
            return false;
        }

        @Nullable
        @Override
        public InteractablePoint getInteractionPoint(Point origin) {
            return null;
        }

        @Override
        public boolean contains(Point point) {
            return false;
        }

        @Override
        public boolean click() {
            return false;
        }

        @Override
        public boolean interact(Pattern action, Pattern target) {
            return false;
        }

        @Override
        public void render(Graphics2D g2d) {
            // no-op
        }

        @Override
        public void render(GraphicsContext gc) {
            // no-op
        }

        @Deprecated
        public enum ContentType {
            ITEM,
            ABILITY;

            @Override
            public String toString() {
                return name().charAt(0) + name().substring(1).toLowerCase();
            }
        }
    }
}
