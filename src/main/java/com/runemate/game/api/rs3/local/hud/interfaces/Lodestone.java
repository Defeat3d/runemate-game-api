package com.runemate.game.api.rs3.local.hud.interfaces;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.rs3.local.hud.*;
import com.runemate.game.api.rs3.local.hud.eoc.*;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.util.*;
import org.jetbrains.annotations.*;

@Deprecated
public enum Lodestone implements Locatable {
    ANACHRONIA,
    ASHDALE,
    BANDIT_CAMP,
    LUNAR_ISLE,
    AL_KHARID,
    ARDOUGNE,
    BURTHORPE,
    CATHERBY,
    DRAYNOR_VILLAGE,
    EDGEVILLE,
    FALADOR,
    LUMBRIDGE,
    PORT_SARIM,
    SEERS_VILLAGE,
    TAVERLEY,
    VARROCK,
    YANILLE,
    CANIFIS,
    EAGLES_PEAK,
    FREMENNIK_PROVINCE,
    KARAMJA,
    OOGLOG,
    TIRANNWN,
    MENAPHOS,
    WILDERNESS_VOLCANO,
    PRIFIDDINAS;

    Lodestone() {
        // no-op
    }

    @Nullable
    public static InterfaceComponent getMinimapButton() {
        return null;
    }

    public static int getAvailableQuickTeleportCharges() {
        return 0;
    }

    public static boolean areQuickTeleportChargesUsedByDefault() {
        return false;
    }

    public static boolean useQuickTeleportChargesByDefault(boolean enable) {
        return false;
    }

    public static boolean isInterfaceOpen() {
        return false;
    }

    public static boolean openInterface() {
        return false;
    }

    public static boolean closeInterface() {
        return false;
    }

    private static boolean isActorTeleporting(final Actor actor) {
        return false;
    }

    private static boolean isActorSteppingOff(final Actor actor) {
        return false;
    }

    public int getTextureId() {
        return 0;
    }

    public boolean isActivated() {
        return false;
    }

    public boolean isUsable() {
        return false;
    }

    public boolean isMembersOnly() {
        return false;
    }

    public boolean teleport() {
        return false;
    }

    public boolean teleport(boolean useQuickTeleports) {
        return false;
    }

    @Override
    public String toString() {
        String name = name().replace("_", " ");
        String[] words = name.split(" ");
        name = "";
        for (String word : words) {
            name += word.charAt(0) + word.substring(1).toLowerCase() + ' ';
        }
        return name.trim();
    }

    @Nullable
    @Override
    /**
     * Gets the position of the lodestone object (which is the destination when you use the teleport)
     */ public Coordinate getPosition() {
        return null;
    }

    @Nullable
    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition() {
        return null;
    }

    @Nullable
    @Override
    public Area.Rectangular getArea() {
        return null;
    }
}
