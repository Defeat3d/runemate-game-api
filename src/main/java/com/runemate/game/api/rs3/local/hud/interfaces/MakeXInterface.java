package com.runemate.game.api.rs3.local.hud.interfaces;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import java.util.function.*;
import org.jetbrains.annotations.*;

@RS3Only
@Deprecated
public final class MakeXInterface {

    private MakeXInterface() {
    }

    /**
     * Closes the MakeXInterface
     *
     * @param keybind whether to use the keybind (escape) or not
     * @return true if interaction is successful and the interface closes
     */
    public static boolean close(boolean keybind) {
        return false;
    }

    /**
     * Closes the MakeXInterface, uses PlayerSense to determine whether or not to use the keybind
     *
     * @return true if interaction is successful and the interface closes
     */
    public static boolean close() {
        return false;
    }

    /**
     * Confirms the current selection, uses PlayerSense to determine
     * whether or not to use the keybind.
     *
     * @return true if interaction is successful and the interface closes
     */
    public static boolean confirm() {
        return false;
    }

    /**
     * Confirms the current selection.
     *
     * @param keybind whether to use the keybind (space) or not
     * @return true if interaction is successful and the interface closes
     */
    public static boolean confirm(boolean keybind) {
        return false;
    }

    /**
     * @return the amount to make currently selected in the MakeXInterface
     */
    public static int getAmount() {
        return 0;
    }

    /**
     * @return the currently selected category
     */
    @Nullable
    public static String getSelectedCategory() {
        return null;
    }

    /**
     * @return the currently selected item
     */
    @Nullable
    public static ItemDefinition getSelectedItem() {
        return null;
    }

    public static boolean isOpen() {
        return false;
    }

    /**
     * @param category a string representing the category
     * @return true if the currently selected category matches the given string, else false
     */
    public static boolean isSelectedCategory(String category) {
        return false;
    }

    /**
     * @param definitionPredicate a Predicate to test the currently selected item against
     * @return true if the currently selected item matches the predicate, else false
     */
    public static boolean isSelectedItem(Predicate<ItemDefinition> definitionPredicate) {
        return false;
    }

    /**
     * @param name a string to test the name of the currently selected item against
     * @return true if the name of the currently selected item matches the given name, else false
     */
    public static boolean isSelectedItem(String name) {
        return false;
    }

    /**
     * Changes the amount to the specified value
     *
     * @param amount   the amount to select
     * @param clickBar whether or not to click the sliding bar
     * @return true if interaction is successful and the new selected
     * amount matches the specified amount.
     */
    public static boolean selectAmount(int amount, boolean clickBar) {
        return false;
    }

    /**
     * Selects the category that matches the given string
     *
     * @param name the name of the category
     * @return true if the category is successfully selected
     */
    public static boolean selectCategory(String name) {
        return false;
    }

    /**
     * Selects the item with a matching name
     *
     * @param name the name to use to determine the item to select
     * @return true if the item is successfully selected
     */
    public static boolean selectItem(String name) {
        return false;
    }

    /**
     * Selects the item that matches the given predicate
     *
     * @param definitionPredicate the predicate to use to determine the item to select
     * @return true if the item is successfully selected
     */
    public static boolean selectItem(Predicate<ItemDefinition> definitionPredicate) {
        return false;
    }

    public static List<ItemDefinition> getItems(Predicate<ItemDefinition> def) {
        return Collections.emptyList();
    }

    public static List<ItemDefinition> getItems() {
        return Collections.emptyList();
    }

    public static List<String> getCategories() {
        return Collections.emptyList();
    }

    private static InterfaceComponent getAmountComponent() {
        return null;
    }

    private static InterfaceComponent getAmountInputComponent() {
        return null;
    }

    private static InterfaceComponent getCategoryOptionComponent(String name) {
        return null;
    }

    private static InterfaceComponent getCloseComponent() {
        return null;
    }

    private static InterfaceComponent getConfirmComponent() {
        return null;
    }

    private static InterfaceComponent getSelectedCategoryComponent() {
        return null;
    }

    private static InterfaceComponent getSelectedItemComponent() {
        return null;
    }
}