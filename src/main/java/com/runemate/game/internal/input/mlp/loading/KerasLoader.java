package com.runemate.game.internal.input.mlp.loading;

import lombok.extern.log4j.*;
import org.jblas.DoubleMatrix;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class KerasLoader {

    private final InputStream weightFile, configFile;

    public KerasLoader(final InputStream weightFile, final InputStream configFile) {
        this.weightFile = weightFile;
        this.configFile = configFile;
    }


    /**
     * Keras Weight file format:
     * The weight format is a JSON array every 2 indexes is a layer
     * The 1st of the 2 indexes are the weights.
     * The weights are comprised of 1 JSON array for every input node, and each node contains the weights for all
     * outgoing synapses
     * The 2nd of the 2 indexes are the biases
     * This is just an array of doubles and the length of the bias array is the size of the next layer
     * <p>
     * Since as of now, this library will only support MLPs we only care out dense layers
     * If we come across a layer that isn't dense, dropout, or leakyrelu we will throw an error
     * I hope to extend this library one day to be a little more robust
     *
     * @return
     */
    public MultilayerPerceptron buildModel() {
        try {
            final String weightContents = this.readStream(this.weightFile);
            final String configContents = this.readStream(this.configFile);

            final List<LayerConfig> layerConfigs = new ArrayList<>();

            final JSONObject confContainer = new JSONObject(configContents);
            final JSONArray configs = confContainer.getJSONArray("config");
            for (int i = 0; i < configs.length(); i++) {
                final JSONObject layer = configs.getJSONObject(i);

                String layerType = layer.getString("class_name");
                if (layerType.equals("Dense")) {

                    final int unitCount = layer.getJSONObject("config").getInt("units");
                    final String activation = layer.getJSONObject("config").getString("activation");
                    layerConfigs.add(new LayerConfig(layerType, activation, unitCount));

                }

            }

            // add each of the weights and biases to layer configs then build

            final JSONArray weightContainer = new JSONArray(weightContents);
            int count = 0;
            for (int i = 0; i < weightContainer.length(); i += 2) {

                JSONArray weights = weightContainer.getJSONArray(i);
                JSONArray biases = weightContainer.getJSONArray(i + 1);

                final int inputSize = weights.length();
                final int outputSize = biases.length();

                final double[][] weightDoubles = new double[outputSize][inputSize];
                for (int j = 0; j < inputSize; j++) {
                    final JSONArray object = weights.getJSONArray(j);
                    for (int k = 0; k < object.length(); k++) {
                        weightDoubles[k][j] = object.getDouble(k);
                    }
                }

                final double[] biasDoubles = new double[outputSize];
                for (int j = 0; j < outputSize; j++) {
                    biasDoubles[j] = biases.getDouble(j);
                }

                final DoubleMatrix weightMatrix = new DoubleMatrix(weightDoubles);
                final DoubleMatrix biasMatrix = new DoubleMatrix(biasDoubles);

                final LayerConfig currentConfig = layerConfigs.get(count);
                currentConfig.setBiases(biasMatrix);
                currentConfig.setWeights(weightMatrix);

                count++;

            }

            return new ModelConfig(layerConfigs).buildModel();

        } catch (Exception e) {
            log.warn("Failed to build model", e);
            return null;
        }
    }

    private String readStream(final InputStream in) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }

        return out.toString();
    }

}
