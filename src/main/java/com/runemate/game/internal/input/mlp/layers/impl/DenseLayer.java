package com.runemate.game.internal.input.mlp.layers.impl;

import com.runemate.game.internal.input.mlp.activation.ActivationFunction;
import com.runemate.game.internal.input.mlp.layers.Layer;
import org.jblas.DoubleMatrix;

public class DenseLayer extends Layer {

    public DenseLayer(DoubleMatrix weights, DoubleMatrix biases, ActivationFunction activationFunction) {
        super(weights, biases);
        super.setActivationFunction(activationFunction);
    }

}
