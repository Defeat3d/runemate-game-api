package com.runemate.game.internal.services;

import com.runemate.client.framework.open.*;
import com.runemate.game.api.client.embeddable.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.ui.*;
import java.util.*;
import java.util.concurrent.*;
import javafx.scene.*;

public class BotServiceProvider implements BotServicesProvider {

    private static final Map<AbstractBot, BotService> services = new ConcurrentHashMap<>();

    @Override
    public BotService getService(AbstractBot abstractBot) {
        return services.computeIfAbsent(abstractBot, BotServiceImpl::new);
    }

    @Override
    public void release(final AbstractBot abstractBot) {
        services.remove(abstractBot);
    }

    @Override
    public EmbeddableUI createDefaultUserInterface(final AbstractBot bot) {
        return new DefaultUI(bot);
    }
}
