package com.runemate.ui.control;

import com.runemate.game.internal.*;
import com.runemate.ui.*;
import java.io.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.image.*;
import lombok.*;
import lombok.experimental.*;

@InternalAPI
@UtilityClass
public class FXUtil {

    @SneakyThrows(IOException.class)
    void loadFxml(@NonNull Object controller, @NonNull String path) {
        final var loader = new FXMLLoader(DefaultUI.class.getResource(path));
        loader.setRoot(controller);
        loader.setController(controller);
        loader.load();
    }

    void loadCss(@NonNull Parent control, @NonNull String path) {
        final var url = DefaultUI.class.getResource(path);
        if (url != null) {
            control.getStylesheets().add(url.toExternalForm());
        }
    }

    Image loadImage(@NonNull String path) {
        final var url = DefaultUI.class.getResource(path);
        try (var input = url.openStream()) {
            return new Image(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
