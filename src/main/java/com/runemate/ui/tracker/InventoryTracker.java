package com.runemate.ui.tracker;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;
import java.util.concurrent.*;
import javafx.application.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import javafx.beans.value.*;
import javafx.collections.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Getter
@Log4j2
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InventoryTracker implements InventoryListener, EquipmentListener, EngineListener {

    final AbstractBot bot;
    final ObservableMap<Integer, ItemTracker> items = FXCollections.observableMap(new ConcurrentHashMap<>());
    final IntegerProperty profit = new SimpleIntegerProperty(0);

    volatile boolean blocking = false;

    ItemEventFilter itemEventFilter = event -> {
        final var item = event.getItem();
        return item != null
            && !blocking
            && (item.getOrigin() == SpriteItem.Origin.INVENTORY
            || item.getOrigin() == SpriteItem.Origin.EQUIPMENT);
    };

    public InventoryTracker(final AbstractBot bot) {
        this.bot = bot;
        bot.getEventDispatcher().addListener(this);
    }

    @Nullable
    private synchronized ItemTracker getTracker(int id) {
        var def = ItemDefinition.get(id);
        if (def == null) {
            return null;
        }
        if (def.isNoted()) {
            id = def.getUnnotedId();
            def = ItemDefinition.get(id);
            if (def == null) {
                return null;
            }
        }
        var item = items.get(id);
        if (item == null) {
            for (final var entry : items.entrySet()) {
                if (Objects.equals(entry.getValue().getName(), def.getName())) {
                    item = entry.getValue();
                    items.put(id, item);
                    break;
                }
            }
        }
        if (item == null) {
            final var lookup = def.isTradeable() ? GrandExchange.lookup(id) : null;
            item = new ItemTracker(id, def.getName(), lookup == null ? def.getShopValue() : lookup.getPrice());
            items.put(id, item);
        }
        return item;
    }

    @Override
    public synchronized void onItemEquipped(final ItemEvent event) {
        //Inverse of inventory operations, so we don't have -1 when we equip an item
        final var item = getTracker(event.getItem().getId());
        if (item != null && accept(event)) {
            Platform.runLater(() -> {
                update(item.quantity, event.getQuantityChange());
                update(profit, item.value * event.getQuantityChange());
            });
        }
    }

    @Override
    public synchronized void onItemUnequipped(final ItemEvent event) {
        //Inverse of inventory operations, so we don't have +1 when we un-equip an item
        final var item = getTracker(event.getItem().getId());
        if (item != null && accept(event)) {
            Platform.runLater(() -> {
                update(item.quantity, -event.getQuantityChange());
                update(profit, item.value * -event.getQuantityChange());
            });
        }
    }

    @Override
    public synchronized void onItemAdded(final ItemEvent event) {
        final var item = getTracker(event.getItem().getId());
        if (item != null && accept(event)) {
            Platform.runLater(() -> {
                update(item.quantity, event.getQuantityChange());
                update(profit, item.value * event.getQuantityChange());
            });
        }
    }

    @Override
    public synchronized void onItemRemoved(final ItemEvent event) {
        final var item = getTracker(event.getItem().getId());
        if (item != null && accept(event)) {
            update(item.quantity, -event.getQuantityChange());
            update(profit, item.value * -event.getQuantityChange());
        }
    }

    private synchronized void update(WritableNumberValue expr, int change) {
        if (expr == null) {
            return;
        }

        try {
            final var current = expr.getValue().intValue();
            expr.setValue(current + change);
        } catch (Exception e) {
            log.warn("Failed to update expression by {}", change, e);
        }
    }

    @Override
    public void onTickStart() {
        blocking = Bank.isOpen() || DepositBox.isOpen() || GrandExchange.isOpen();
    }

    public void setItemEventFilter(final ItemEventFilter filter) {
        this.itemEventFilter = filter;
    }

    private boolean accept(@NonNull ItemEvent event) {
        return itemEventFilter.test(event);
    }

    @Value
    public static class ItemTracker {

        int id;
        String name;
        int value;
        IntegerProperty quantity = new SimpleIntegerProperty(0);
        BooleanBinding showing = quantity.isNotEqualTo(0);
    }
}
