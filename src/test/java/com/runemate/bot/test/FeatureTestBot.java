package com.runemate.bot.test;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import java.lang.reflect.*;
import java.util.*;
import javafx.beans.property.*;
import javafx.scene.control.*;
import lombok.extern.log4j.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 * <p>
 * The following gradle command will build this bot and then launch the client:
 * testJar launch --args="--dev"
 */
@Log4j2
public class FeatureTestBot extends LoopingBot implements SettingsListener, NpcListener {

    @SettingsProvider(updatable = true)
    private ExampleSettings settings;

    @Override
    public void onStart(final String... arguments) {
        setLoopDelay(1000);
    }

    @Override
    public void onLoop() {
        Player local = Players.getLocal();
        if (local == null) {
            return;
        }

        Coordinate pos = local.getPosition();
        Coordinate uninstanced = Coordinate.uninstance(pos);
        if (pos != null && uninstanced != null) {
//            log.info("Player: {}", pos);
//            log.info("Uninstanced: {}", uninstanced);

            Coordinate instanced = Coordinate.instance(uninstanced);
            log.info("Instanced: {}", instanced);


            Chunk chunk = Chunk.containing(pos);
            if (chunk != null) {
//                log.info("Chunk: {}", chunk);
                log.info("Chunk instanced: {}", chunk.instance(uninstanced));
            }
        }
    }

    @Override
    public void onNpcDespawned(final NpcDespawnedEvent event) {
        try {
            log.info("event {}", event);
        } catch (Exception e) {
            log.warn("Failed to read event");
        }
    }

    @Override
    public void onNpcDefinitionChanged(final NpcDefinitionChangedEvent event) {
        try {
            log.info("event {}", event);
        } catch (Exception e) {
            log.warn("Failed to read event");
        }
    }

    public static class TestNodeProducer implements CustomSettingControlProducer<UUID, TestNode> {

        @Override
        public TestNode produce(AbstractBot bot, SettingsManager manager, SettingDescriptor desc) {
            return new TestNode();
        }
    }

    public static class TestNode extends Button implements CustomSettingControl<UUID> {

        private final ObjectProperty<UUID> uuid = new SimpleObjectProperty<>(UUID.randomUUID());

        public TestNode() {
            textProperty().bind(uuid.asString());
            uuid.addListener((o, o1, n) -> log.info("New UUID {}", n));
            setOnAction(e -> uuid.set(UUID.randomUUID()));
        }

        @Override
        public Property<UUID> valueProperty() {
            return uuid;
        }
    }

    public static class UUIDConverter implements SettingConverter {

        @Override
        public Object fromString(final String s, final Type type) throws Exception {
            if (type.equals(UUID.class)) {
                return UUID.fromString(s);
            }
            return null;
        }

        @Override
        public String toString(final Object o) {
            return o.toString();
        }
    }

    @Override
    public void onSettingChanged(final SettingChangedEvent e) {
        log.info("{}.{} -> {}", e.getGroup(), e.getKey(), e.getValue());
    }

    @Override
    public void onSettingsConfirmed() {

    }
}
